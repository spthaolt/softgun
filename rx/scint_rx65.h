/**
 ******************************************************************************
 * Software configurable interrupts for Renesas RX65
 ******************************************************************************
 */
#include "bus.h"
BusDevice *RX65Scint_New(const char *name, const char *icuname);
