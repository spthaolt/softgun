/*
 */

#include "bus.h"
#include "sgstring.h"
#include "clock.h"
#include "diskimage.h"
#include "configfile.h"
#include "reset_rx63.h"
#include "sglib.h"

#define REG_RSTR0(base)     (0x0008C290) 
#define REG_RSTR1(base)     (0x0008C291)
#define REG_RSTR2(base)     (0x000800C0)
#define REG_SWRR(base)      (0x000800C2)

typedef struct RxReset {
    BusDevice bdev;
    uint8_t regRSTR0;
    uint8_t regRSTR1;
    uint8_t regRSTR2;
    uint8_t regSWRR;
} RxReset;

static void
rstr0_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
rstr0_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}

static void
rstr1_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
rstr1_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}

static void
rstr2_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
rstr2_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}


static void
swrr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxReset *rst = clientData;
    rst->regSWRR = value;
    if (value == 0xa501) {
        fflush(stdout);
        fflush(stderr);
        usleep(10000);
        fprintf(stderr,"\nSoftware Reset\n");
        usleep(10000);
        exit(1);
    }
}

static uint32_t
swrr_read(void *clientData, uint32_t address, int rqlen)
{
    RxReset *rst = clientData;
    return rst->regSWRR;
}

static void
RxReset_Unmap(void *owner, uint32_t base, uint32_t mask)
{
    //IOH_Delete8(REG_ODR(base, i));
    IOH_Delete8(REG_RSTR0(base));
    IOH_Delete8(REG_RSTR1(base));
    IOH_Delete8(REG_RSTR2(base));
    IOH_Delete16(REG_SWRR(base));
}

static void
RxReset_Map(void *owner, uint32_t base, uint32_t mask, uint32_t mapflags)
{
    RxReset *rst = owner;
    IOH_New8(REG_RSTR0(base), rstr0_read, rstr0_write, rst);
    IOH_New8(REG_RSTR1(base), rstr1_read, rstr1_write, rst);
    IOH_New8(REG_RSTR2(base), rstr2_read, rstr2_write, rst);
    IOH_New16(REG_SWRR(base), swrr_read, swrr_write, rst);
}


BusDevice *
Rx63Reset_New(const char *name) 
{
    RxReset *rst = sg_new(RxReset);
    rst->bdev.first_mapping = NULL;
    rst->bdev.Map = RxReset_Map;
    rst->bdev.UnMap = RxReset_Unmap;
    rst->bdev.owner = rst;
    rst->bdev.hw_flags = MEM_FLAG_WRITABLE | MEM_FLAG_READABLE;
    return &rst->bdev;
}

