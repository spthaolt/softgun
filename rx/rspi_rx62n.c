/**
 *For rx63 read: r01an1105ej0101
 */

#include <stdint.h>
#include "clock.h"
#include "cycletimer.h"
#include "rspi_rx62n.h"
#include "sgstring.h"
#include "bus.h"
#include "signode.h"

#define REG_SPCR(base)      ((base) + 0x00)
#define     SPCR_SPMS       (1 << 0)
#define     SPCR_TXMD       (1 << 1)
#define     SPCR_MODFEN     (1 << 2)
#define     SPCR_MSTR       (1 << 3)
#define     SPCR_SPEIE      (1 << 4)
#define     SPCR_SPTIE      (1 << 5)
#define     SPCR_SPE        (1 << 6)
#define     SPCR_SPRIE      (1 << 7)

#define REG_SSLP(base)      ((base) + 0x01)
#define     SSL0P           (1 << 0)
#define     SSL1P           (1 << 1)
#define     SSL2P           (1 << 2)
#define     SSL3P           (1 << 3)

#define REG_SPPCR(base)     ((base) + 0x02) 
#define     SPPCR_SPLP      (1 << 0)
#define     SPPCR_SPLP2     (1 << 1)
#define     SPPCR_SPOM      (1 << 2)
#define     SPPCR_MOIFV     (1 << 4) 
#define     SPPCR_MOIFE     (1 << 5)

#define REG_SPSR(base)      ((base) + 0x03)
#define     SPSR_OVRF       (1 << 0)
#define     SPSR_IDLNF      (1 << 1)
#define     SPSR_MODF       (1 << 2)
#define     SPSR_PERF       (1 << 3)
#define     SPSR_SPTEF      (1 << 5)
#define     SPSR_SPRF       (1 << 7)
#define REG_SPDR(base)      ((base) + 0x04)
#define REG_SPSCR(base)     ((base) + 0x08)
#define     SPSCR_SPSLN_MSK (7)
#define REG_SPSSR(base)     ((base) + 0x09)
#define     SPSSR_SPCP_MSK      (7)
#define     SPSSR_SPECM_MSK     (7 << 4) 
#define REG_SPBR(base)      ((base) + 0x0a)
#define REG_SPDCR(base)     ((base) + 0x0b)
#define     SPDCR_SPFC_MSK      (3 << 0)
#define     SPDCR_SPFC_SLSEL    (3 << 2)
#define     SPDCR_SPRDTD        (1 << 4)
#define     SPDCR_SPLW          (1 << 5)
#define REG_SPCKD(base)     ((base) + 0x0c)
#define     SPCKD_SCKDL_MSK     (7 << 0)
#define REG_SSLND(base)     ((base) + 0x0d)
#define     SSLND_SLNDL_MSK     (7 << 0)
#define REG_SPND(base)      ((base) + 0x0e)
#define      SPND_SPNDL_MSK     (7 << 0)
#define REG_SPCR2(base)     ((base) + 0x0f)
#define     SPCR2_SPPE      (1 << 0)
#define     SPCR2_SPOE      (1 << 1)
#define     SPCR2_SPIIE     (1 << 2)
#define     SPCR2_PTE       (1 << 3)
#define REG_SPCMD0(base)    ((base) + 0x10)
#define     SPCMD_CPHA       (1 << 0)
#define     SPCMD_CPOL       (1 << 1)
#define     SPCMD_BRDV_MSK   (3 << 2)
#define     SPCMD_BRDV_SHIFT (2)
#define     SPCMD_SSLA_MSK   (3 << 4)
#define     SPCMD_SSLKP      (1 << 7) 
#define     SPCMD_SPB_MSK    (0xf << 8) 
#define     SPCMD_SPB_SHIFT  (8) 
#define     SPCMD_LSBF       (1 << 12)
#define     SPCMD_SPNDEN     (1 << 13)
#define     SPCMD_SLNDEN     (1 << 14)
#define     SPCMD_SCKDEN     (1 << 15)
#define REG_SPCMD1(base)    ((base) + 0x12)
#define REG_SPCMD2(base)    ((base) + 0x14)
#define REG_SPCMD3(base)    ((base) + 0x16)
#define REG_SPCMD4(base)    ((base) + 0x18)
#define REG_SPCMD5(base)    ((base) + 0x1A)
#define REG_SPCMD6(base)    ((base) + 0x1C)
#define REG_SPCMD7(base)    ((base) + 0x1E)

#define ICODE_SIZE  (512)

#define INSTR_MOSI_HIGH     UINT32_C(0x01000000)
#define INSTR_MOSI_LOW      UINT32_C(0x02000000)
#define INSTR_MOSI_OPEN     UINT32_C(0x03000000)
#define INSTR_MISO_HIGH     UINT32_C(0x04000000)
#define INSTR_MISO_LOW      UINT32_C(0x05000000)
#define INSTR_MISO_OPEN     UINT32_C(0x06000000)
#define INSTR_CLK_HIGH      UINT32_C(0x07000000)
#define INSTR_CLK_LOW       UINT32_C(0x08000000)
#define INSTR_CS_HIGH(csNr) (UINT32_C(0x09000000) | (csNr))
#define INSTR_CS_LOW(csNr)  (UINT32_C(0x0a000000) | (csNr))
#define INSTR_NDELAY(ns)    (UINT32_C(0x0b000000) | (ns))
#define INSTR_CLKDELAY(cyc) (UINT32_C(0x0c000000) | (cyc))
#define INSTR_SHIFTOUT_MOSI (UINT32_C(0x0d000000))
#define INSTR_SAMPLE_MOSI   (UINT32_C(0x0e000000))
#define INSTR_SAMPLE_MISO       (UINT32_C(0x10000000)) 
#define INSTR_TRANSFER_RXSHIFT  (UINT32_C(0x11000000)) 
#define INSTR_RELOAD_TXSHIFT  (UINT32_C(0x12000000))
#define INSTR_TRIGGER_SPRI    (UINT32_C(0x13000000))
#define INSTR_TRIGGER_SPTI    (UINT32_C(0x14000000))
#define INSTR_TRIGGER_SPII    (UINT32_C(0x15000000))
#define INSTR_ENDSCRIPT         (UINT32_C(0x16000000))

#define RXBUF_LONGWORDS UINT32_C(4)
#define TXBUF_LONGWORDS UINT32_C(4)
#define RXBUF_WP(rspi) ((rspi)->rxBufWp % ((rspi->regSPDCR & 3) + 1))
#define RXBUF_RP(rspi) ((rspi)->rxBufRp & (RXBUF_LONGWORDS - 1))
#define TXBUF_WP(rspi) ((rspi)->txBufWp & (TXBUF_LONGWORDS - 1))
#define TXBUF_RP(rspi) ((rspi)->txBufRp & (TXBUF_LONGWORDS - 1))


typedef struct RxRSpi {
    BusDevice bdev;
    uint32_t icode[ICODE_SIZE];
    uint32_t instrP;
    uint32_t instrWp;
    Clock_t *clkIn;
    Clock_t *clkBase; /* Rate behind divisor by SPBR is called Base Bitrate in 38.2.14 */
    CycleTimer instrDelayTimer;

    SigNode *sigMosi;
    SigNode *sigMiso;

#if 0
    SigNode *sigShiftOut; /* Connected to MOSI in master mode or to Miso in Slave mode */
    SigNode *sigShiftIn;
#endif

    SigNode *sigSclk;
    SigNode *sigSSL[4];
    SigNode *sigIrqSpti; /* SPI TX interrupt */
    SigNode *sigIrqSpri; /* SPI RX interrupt */
    SigNode *sigIrqSpii; /* SPI Idle interrupt */
    SigNode *sigIrqSpei; /* SPI Error interrupt */

    uint32_t txShiftReg;    /* Maybe uint64_t because of parity bit ? */
    uint32_t txShiftCnt;
    uint32_t txShiftEmpty;
    uint32_t txBuf32[4];

    uint32_t rxBuf32[4];
    uint32_t txBufWp;
    uint32_t txBufRp;
    uint32_t rxShiftReg;
    uint32_t rxBufWp;
    uint32_t rxBufRp;
    uint8_t regSPCR;
    uint8_t regSSLP;
    uint8_t regSPPCR; 
    uint8_t regSPSR;
    uint8_t regSPSCR;
    uint8_t regSPSSR;
    uint8_t regSPBR; 
    uint8_t regSPDCR;
    uint8_t regSPCKD;
    uint8_t regSSLND;
    uint8_t regSPND;
    uint8_t regSPCR2;
    uint16_t regSPCMD[8];
} RxRSpi;

/**
 **************************************************************************
 * Translate the spd RSPI Data Length Setting field to a number of Bits
 **************************************************************************
 */
static const uint8_t gSpbToLength[16] = {
    /* 0  */ 20, 
    /* 1  */ 24, 
    /* 2  */ 32,
    /* 3  */ 32,
    /* 4  */  8,
    /* 5  */  8,
    /* 6  */  8,
    /* 7  */  8,
    /* 8  */  9,
    /* 9  */ 10,
    /* 10 */ 11,
    /* 11 */ 12,
    /* 12 */ 13,
    /* 13 */ 14,
    /* 14 */ 15,
    /* 15 */ 16,
};

/**
 *************************************************************
 * \fn static void TriggerTxInterrupt(RxRSpi *rspi); 
 *************************************************************
 */
static void
TriggerTxInterrupt(RxRSpi *rspi) {
    SigNode_Set(rspi->sigIrqSpti, SIG_LOW);
    SigNode_Set(rspi->sigIrqSpti, SIG_HIGH);
}

static void
TriggerRxInterrupt(RxRSpi *rspi) {
    SigNode_Set(rspi->sigIrqSpri, SIG_LOW);
    SigNode_Set(rspi->sigIrqSpri, SIG_HIGH);
}

static void
TriggerIdleInterrupt(RxRSpi *rspi) {
    SigNode_Set(rspi->sigIrqSpii, SIG_LOW);
    SigNode_Set(rspi->sigIrqSpii, SIG_HIGH);
}

static bool 
RxSpi_EvalInstr(RxRSpi *rspi) 
{
    uint32_t icode;
    uint32_t instr, arg;
    bool retval = false;
    if (rspi->instrP >= array_size(rspi->icode)) {
        fprintf(stderr, "Illegal instruction pointer in %s\n", __func__); 
        /* Returns true if no more instructions */
        return true;
    } 
    icode = rspi->icode[rspi->instrP];
    instr = icode & UINT32_C(0xff000000); 
    arg = icode & 0xffffff;
    rspi->instrP++;
    switch (instr) {
        case INSTR_MOSI_HIGH:
            SigNode_Set(rspi->sigMosi, SIG_HIGH);
            break;

        case INSTR_MOSI_LOW:
            SigNode_Set(rspi->sigMosi, SIG_LOW);
            break;

        case INSTR_MOSI_OPEN:
            SigNode_Set(rspi->sigMosi, SIG_OPEN);
            break;

        case INSTR_MISO_HIGH:
            SigNode_Set(rspi->sigMiso, SIG_HIGH);
            break;

        case INSTR_MISO_LOW:
            SigNode_Set(rspi->sigMiso, SIG_LOW);
            break;

        case INSTR_MISO_OPEN:
            SigNode_Set(rspi->sigMiso, SIG_OPEN);
            break;

        case INSTR_CLK_HIGH:
            SigNode_Set(rspi->sigSclk, SIG_HIGH);
            break;

        case INSTR_CLK_LOW:
            SigNode_Set(rspi->sigSclk, SIG_LOW);
            break;

        case INSTR_CS_HIGH(0):
            if (arg < array_size(rspi->sigSSL)) {
                SigNode_Set(rspi->sigSSL[arg], SIG_HIGH);
            }
            break;

        case INSTR_CS_LOW(0):
            if (arg < array_size(rspi->sigSSL)) {
                SigNode_Set(rspi->sigSSL[arg], SIG_LOW);
            }
            break;

        case INSTR_NDELAY(0):
            CycleTimer_Mod(&rspi->instrDelayTimer, NanosecondsToCycles(arg));
            break;

        /* 
         *********************************************************************************
         * Delay by nubmer of base clock cycles 
         * The SPCMD specific multiplication is done at compile time.
         *********************************************************************************
         */
        case INSTR_CLKDELAY(0):
            {
                uint64_t cycles; 
                FractionU64_t frac;
                frac = Clock_MasterRatio(rspi->clkBase);
                cycles = arg * frac.denom / frac.nom;
                CycleTimer_Mod(&rspi->instrDelayTimer, cycles);
            }
            retval = true;
            break;

        /* Bit order is converted on overtake from shiftreg to fifo */
        case INSTR_SAMPLE_MOSI:
            {
                if (SigNode_Val(rspi->sigMiso) == SIG_HIGH) {
                    rspi->rxShiftReg  = (rspi->rxShiftReg << 1) | 1;
                } else {
                    rspi->rxShiftReg  = (rspi->rxShiftReg << 1) | 0;
                }
            }
            break;

        /* MSB First variant shifts in on the right side */ 
        case INSTR_SAMPLE_MISO:
            {
                if (SigNode_Val(rspi->sigMiso) == SIG_HIGH) {
                    rspi->rxShiftReg  = (rspi->rxShiftReg << 1) | 1;
                } else {
                    rspi->rxShiftReg  = (rspi->rxShiftReg << 1) | 0;
                }
            }
            break;

        case INSTR_TRANSFER_RXSHIFT:
            break;
        
        case INSTR_RELOAD_TXSHIFT: 
            break;

        case INSTR_TRIGGER_SPRI:
            TriggerRxInterrupt(rspi);
            break;

        case INSTR_TRIGGER_SPTI:
            TriggerTxInterrupt(rspi);
            break;

        case INSTR_TRIGGER_SPII:
            TriggerIdleInterrupt(rspi);
            break;

        case INSTR_ENDSCRIPT:
            retval = true;
            break;

        default:
            break;
    }
    return retval;
}

static void
RxSpi_InterpLoop(void *eventData) 
{
    RxRSpi *rspi = eventData;
    bool done;
    do {
        done = RxSpi_EvalInstr(rspi);
    } while (done == false);
}

static void
update_rx_interrupt(RxRSpi *rspi) 
{
    //
    // if(rspi->regSPCR & SPCR_TI) {};
    //
}

#if 0
static void
update_tx_interrupt(RxRSpi *rspi)
{
    /* 
     */
}
#endif

static void
InstrAppend(RxRSpi *rspi, uint32_t icode)
{
    if (rspi->instrWp < array_size(rspi->icode)) { 
        rspi->icode[rspi->instrWp] = icode;
        rspi->instrWp++;
    } else {
        fprintf(stderr, "RX-RSpi Icode to long\n");
    }
}

static void
ScriptClear(RxRSpi *rspi, uint32_t icode)
{
    rspi->instrWp = 0;
    // Timer_Cancel
}

static void
spcmd_asm_master_script(RxRSpi *rspi)
{
    uint32_t nrBits;
    uint32_t i;
    //unsigned int spb;
    uint32_t spiCmdPtr;
    bool sslkp;
    uint16_t spiCmd;
    bool cpol, cpha;
    uint32_t brdiv;
    uint8_t regSSLP;
    uint8_t spms;
    uint8_t ssla;
    uint8_t txmd;
    uint8_t sslpol;
    spiCmdPtr = 0; /* TODO, use real command pointer. This is a dummy */
    spiCmd = rspi->regSPCMD[spiCmdPtr]; 
    cpha = !!(spiCmd & SPCMD_CPHA);
    cpol = !!(spiCmd & SPCMD_CPOL);
    sslkp = !!(spiCmd & SPCMD_SSLKP);
    spms = !!(rspi->regSPCR & SPCR_SPMS);
    ssla = (spiCmd >> 4) & 7;
    brdiv = 1 << ((spiCmd & SPCMD_BRDV_MSK) >> SPCMD_BRDV_SHIFT);
    nrBits = gSpbToLength[(spiCmd & SPCMD_SPB_MSK) >> SPCMD_SPB_SHIFT];
    regSSLP = rspi->regSSLP;
    sslpol = (regSSLP >> ssla) & 1;
    txmd  = !!(rspi->regSPCR & SPCR_TXMD);
    if (ssla > 3) {
        fprintf(stderr, "SSLA out of range: %u\n", ssla);
        ssla &= 0x3;
    }
    InstrAppend(rspi, INSTR_RELOAD_TXSHIFT);
    InstrAppend(rspi, INSTR_TRIGGER_SPTI); /* After loading the shift register trigger an TX empty interrupt */ 
    if (spms == 0) {
        if (sslpol == 0) {
          InstrAppend(rspi, INSTR_CS_LOW(ssla));
        } else {
          InstrAppend(rspi, INSTR_CS_HIGH(ssla));
        }
    }
    if (cpol == 0) {
        InstrAppend(rspi, INSTR_CLK_LOW);
    } else {
        InstrAppend(rspi, INSTR_CLK_HIGH);
    }
    for (i = 0; i < nrBits; i++) {
        /* First comes the odd EDGE */ 
        if (cpha == 0) {
            InstrAppend(rspi, INSTR_SAMPLE_MISO);
        } else {
            InstrAppend(rspi, INSTR_SHIFTOUT_MOSI);
        }
        if (cpol == 0) {
            InstrAppend(rspi, INSTR_CLK_HIGH);
        } else {
            InstrAppend(rspi, INSTR_CLK_LOW);
        }
        //InstrAppend(rspi, INSTR_HALFCLOCKDELAY);
        /* Now the even edge */
        if (cpha == 1) {
            InstrAppend(rspi, INSTR_SAMPLE_MISO);
        } else {
            InstrAppend(rspi, INSTR_SHIFTOUT_MOSI);
        }
        //InstrAppend(rspi, INSTR_HALFCLOCKDELAY);
        if (cpol == 0) {
            InstrAppend(rspi, INSTR_CLK_LOW);
        } else {
            InstrAppend(rspi, INSTR_CLK_HIGH);
        }
    }
    if (cpol == 0) {
        // InstrAppend(rspi, INSTR_DELAY(t2));
        // InstrAppend(rspi, INSTR_RELEASE_MOSI);
        if (sslkp) {
            if (spms == 0) {
                if (sslpol == 0) {
                  InstrAppend(rspi, INSTR_CS_HIGH(ssla));
                } else {
                  InstrAppend(rspi, INSTR_CS_LOW(ssla));
                }
            }
        }
    } else {
        // InstrAppend(rspi, INSTR_DELAY(t2));
        // InstrAppend(rspi, INSTR_RELEASE_MOSI);
        if (sslkp) {
            if (spms == 0) {
                if (sslpol == 0) {
                  InstrAppend(rspi, INSTR_CS_HIGH(ssla));
                } else {
                  InstrAppend(rspi, INSTR_CS_LOW(ssla));
                }
            }
        } 
    }
    // InstrAppend(rspi, INSTR_DELAY(t3));
    if (txmd == 0) {
       InstrAppend(rspi, INSTR_TRIGGER_SPRI);
    }
    //InstrAppend(rspi, INSTR_RELOAD_TXSHIFT);    
}
/**
 **********************************************************
 * \fn static void spe_clear(RxRSpi *rspi) 
 * Suspend any serial transfer, 
 * Stop driving the output signals in slave mode.
 * Initialize the internal state ????
 * Initialize the transmit buffer
 **********************************************************
 */
static void
spe_clear(RxRSpi *rspi) 
{
    
}

static void
spe_set(RxRSpi *rspi) 
{
    
}
/**
 ********************************************************************
 * RSPI Control Register
 * Bit 7 SPRIE RSPI Receive Interrupt Enable 
 * Bit 6 SPE RSPI Function Enable
 * Bit 5 SPTIE RSPI Tranmit Interrupt Enable
 * Bit 4 SPEIE RSPI Error Interrupt Enable
 * Bit 3 MSTR RSPI Master Slave Mode select.
 * Bit 2 MODFEN Mode Fault Error Detection Enable 
 * Bit 1 TXMD Communications Operating Mode Select. Full duplex/transmit only
 ********************************************************************
 */
static uint32_t
spcr_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCR;
}

/*
 *****************************************************************************
 * Immediate triggering of TX interrupt when SPCR is changed:
 *
 * HAE-01 > rspi1 spti 
 * Transition SPE/SPTIE -> SPE/SPTIE
 *   0 -> 0: 0 
 *   0 -> 1: 0
 *   0 -> 2: 0
 *   0 -> 3: 1
 *   1 -> 0: 0
 *   1 -> 1: 0
 *   1 -> 2: 0
 *   1 -> 3: 1
 *   2 -> 0: 0
 *   2 -> 1: 1
 *   2 -> 2: 0
 *   2 -> 3: 0
 *   3 -> 0: 0
 *   3 -> 1: 1
 *   3 -> 2: 0
 *   3 -> 3: 0
 * 
 *****************************************************************************
 */

static void
spcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    uint8_t diff;
    diff = rspi->regSPCR ^ value;
    /* 
     ***********************************************************************
     * Enabling SPE together with SPTIE triggers an TX empty interrupt.
     ***********************************************************************
     */
    if ((diff & SPCR_SPE) && (value & SPCR_SPTIE)) {
        TriggerTxInterrupt(rspi); 
    }
    rspi->regSPCR = value;
    if (diff & SPCR_SPE) {
        if (value & SPCR_SPE) {
            spe_set(rspi);
        } else {
            spe_clear(rspi);
        }
    }
//    update_tx_interrupt(rspi);
    update_rx_interrupt(rspi);
}

/**
 ***************************************************************
 * Slave select polarity register
 ***************************************************************
 */
static uint32_t
sslp_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSSLP;
}

/* 
 ***********************************************************************************************
 * \fn static void sslp_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
 * Set polatity of chip select signals.
 * Change of polarity when during active controller (SPE == 1) may cause problems. 
 ***********************************************************************************************
 */
static void
sslp_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    int i;
    uint8_t diff = rspi->regSSLP ^ value;
    if (rspi->regSPCR & SPCR_SPE) {
        fprintf(stderr, "Warning: Chip select polarity change while SPI is enabled\n");
    }
    rspi->regSSLP = value & 0xf;
    for (i = 0; i < 4; i++) {
        if (!(diff &  (1 << i))) {
            continue;
        }
        if((value >> i) & 1) {
            SigNode_Set(rspi->sigSSL[i], SIG_LOW);
        } else {
            SigNode_Set(rspi->sigSSL[i], SIG_HIGH);
        }
    }
}

/*
 **********************************************************************************************
 * SPPCR
 * Spi Pin Control register 
 * Bit 0 SPLP  RSPI Loopback mode
 * Bit 1 SPLP2 Loopback mode 2 (transmit data = received data)
 * Bit 3 MOIV  Mosi Idle Fixed Value
 * Bit 4 MOIFE Mosi Idle Value Fixing Enable
 **********************************************************************************************
 */
static uint32_t
sppcr_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPPCR;
}

static void
sppcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    if (value & SPPCR_SPLP) {
#if 0
            SigNode_RemoveLink(rspi->sigMosi, rspi->sigShiftOut);
            SigNode_RemoveLink(rspi->sigMiso, rspi->sigShiftOut);
            SigNode_RemoveLink(rspi->sigMosi, rspi->sigShiftIn);
            SigNode_RemoveLink(rspi->sigMiso, rspi->sigShiftIn);
#endif
    } else if (value & SPPCR_SPLP2) {
#if 0
            SigNode_RemoveLink(rspi->sigMosi, rspi->sigShiftOut);
            SigNode_RemoveLink(rspi->sigMiso, rspi->sigShiftOut);
            SigNode_RemoveLink(rspi->sigMosi, rspi->sigShiftIn);
            SigNode_RemoveLink(rspi->sigMiso, rspi->sigShiftIn);
#endif
    }
    rspi->regSPPCR = value & 0x37;
}

/**
 ***************************************************************************************
 * RSPI Status Register SPSR
 ***************************************************************************************
 */
static uint32_t
spsr_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPSR;
}

static void
spsr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    uint8_t mask = value | 0xf2;
    rspi->regSPSR = rspi->regSPSR & mask;
}

/*
 **************************************************************************
 * Reading the receive buffer switches the receiver buffer read pointer 
 * to the next buffer automatically. The transmit buffer read pointer is not
 * updated when reading from the transmit buffer.
 * When reading from the transmit buffer the value most recently written
 * is read. After generation of tbufEmtpy Interrupt 0 is read.
 **************************************************************************
 */
static uint32_t
spdr_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    uint32_t spdrValue;
    bool splw = rspi->regSPDCR & SPDCR_SPLW; /* true means longword access */
    unsigned int spfc = rspi->regSPDCR & 3;  /* 0-3 is 1 to 4 frames */
    uint32_t nrStages = spfc + 1;
    if (rspi->regSPDCR & SPDCR_SPRDTD) {
        spdrValue = rspi->txBuf32[(rspi->txBufWp + nrStages - 1) % nrStages];
    } else {
        int rxBufNr = RXBUF_RP(rspi);
        spdrValue = rspi->rxBuf32[rxBufNr]; 
        rspi->rxBufRp = (rspi->rxBufRp + 1) % nrStages;
    }
    return spdrValue;
}

/**
 *******************************************************************************
 *******************************************************************************
 */
static void
spdr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData; 
    uint32_t nrStages;
    bool splw;
    //uint32_t nrBits;
    splw = rspi->regSPDCR & SPDCR_SPLW; /* true means longword access */
    nrStages = (rspi->regSPDCR & 3) + 1;
    //nrBits = gSpbToLength[(spiCmd & SPCMD_SPB_MSK) >> SPCMD_SPB_SHIFT];
    rspi->txBuf32[TXBUF_WP(rspi)] = value;
    if (rspi->txBufWp < nrStages) {
        rspi->txBufWp = rspi->txBufWp + 1;
    } else {
        rspi->txBufWp = 0; 
    }
}

/**
 *********************************************************************************
 * SPSCR
 * RSPI Sequence control register. Specifies the sequence length pf the SPCMD's 
 * 0-7 means usage of SPCMD0 only to usage of SPCMD0-7.
 *********************************************************************************
 */
static uint32_t
spscr_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}

static void
spscr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
}

/**
 ************************************************************************************
 * \fn static uint32_t spssr_read(void *clientData, uint32_t address, int rqlen)
 ************************************************************************************
 */
static uint32_t
spssr_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}

/**
 ***********************************************************************
 * RSPI Sequence Status Register
 * Bit 0-2 SPCP Indicates the number of the current SPCMD (0-7).
 * Bit 4-6 RSPI Error command indicates the SPCMD which caused an error 
 ***********************************************************************
 */
static void
spssr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
}

static uint32_t
spbr_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPBR;
}

/**
 ***************************************************************************
 * SPBR
 * RSPI Bit Rate register 
 ***************************************************************************
 */
static void
spbr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPBR = value;
    Clock_MakeDerived(rspi->clkBase, rspi->clkIn, 1, 2 * (value + 1));
}

/**
 *****************************************************************************
 * RSPI Data control register
 *****************************************************************************
 */
static uint32_t
spdcr_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPDCR;
}

static void
spdcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPDCR = value & 0x33;
}

static uint32_t
spckd_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCKD;
}

static void
spckd_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCKD = value & 0x7;
}

static uint32_t
sslnd_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSSLND;
}

static void
sslnd_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSSLND = value & 0x7;
}

static uint32_t
spnd_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPND; 
}

static void
spnd_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPND = value & 7;
}

static uint32_t
spcr2_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCR2;
}

static void
spcr2_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCR2 = value & 0xf;
}

static uint32_t
spcmd0_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[0];
}

static void
spcmd0_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[0] = value;
}

static uint32_t
spcmd1_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[1];
}

static void
spcmd1_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[1] = value;
}

static uint32_t
spcmd2_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[2];
}

static void
spcmd2_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[2] = value;
}

static uint32_t
spcmd3_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[3];
}

static void
spcmd3_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[3] = value;
}

static uint32_t
spcmd4_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[4];
}

static void
spcmd4_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[4] = value;
}

static uint32_t
spcmd5_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[5];
}

static void
spcmd5_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[5] = value;
}

static uint32_t
spcmd6_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[6];
}

static void
spcmd6_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[6] = value;
}

static uint32_t
spcmd7_read(void *clientData, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    return rspi->regSPCMD[7];
}

static void
spcmd7_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    RxRSpi *rspi = clientData;
    rspi->regSPCMD[7] = value;
}

static void
Rspi_Unmap(void *owner, uint32_t base, uint32_t mask)
{
    IOH_Delete8(REG_SPCR(base));
    IOH_Delete8(REG_SSLP(base));
    IOH_Delete8(REG_SPPCR(base));
    IOH_Delete8(REG_SPSR(base));
    IOH_Delete32(REG_SPDR(base));
    IOH_Delete8(REG_SPSCR(base));
    IOH_Delete8(REG_SPSSR(base));
    IOH_Delete8(REG_SPBR(base));
    IOH_Delete8(REG_SPDCR(base));
    IOH_Delete8(REG_SPCKD(base));
    IOH_Delete8(REG_SSLND(base));
    IOH_Delete8(REG_SPND(base));
    IOH_Delete8(REG_SPCR2(base));
    IOH_Delete16(REG_SPCMD0(base));
    IOH_Delete16(REG_SPCMD1(base));
    IOH_Delete16(REG_SPCMD2(base));
    IOH_Delete16(REG_SPCMD3(base));
    IOH_Delete16(REG_SPCMD4(base));
    IOH_Delete16(REG_SPCMD5(base));
    IOH_Delete16(REG_SPCMD6(base));
    IOH_Delete16(REG_SPCMD7(base));
}

/**
 *********************************************************************************************
 * \fn static void Rspi_Map(void *owner, uint32_t base, uint32_t mask, uint32_t mapflags)
 *********************************************************************************************
 */
static void
Rspi_Map(void *owner, uint32_t base, uint32_t mask, uint32_t mapflags)
{
    RxRSpi *rspi = owner;
    IOH_New8(REG_SPCR(base), spcr_read, spcr_write, rspi);
    IOH_New8(REG_SSLP(base),sslp_read,sslp_write,rspi);
    IOH_New8(REG_SPPCR(base),sppcr_read,sppcr_write,rspi);
    IOH_New8(REG_SPSR(base),spsr_read,spsr_write,rspi);
    IOH_New32(REG_SPDR(base),spdr_read,spdr_write,rspi);
    IOH_New8(REG_SPSCR(base),spscr_read,spscr_write,rspi);
    IOH_New8(REG_SPSSR(base),spssr_read,spssr_write,rspi);
    IOH_New8(REG_SPBR(base),spbr_read,spbr_write,rspi);
    IOH_New8(REG_SPDCR(base),spdcr_read,spdcr_write,rspi);
    IOH_New8(REG_SPCKD(base),spckd_read,spckd_write,rspi);
    IOH_New8(REG_SSLND(base),sslnd_read,sslnd_write,rspi);
    IOH_New8(REG_SPND(base),spnd_read,spnd_write,rspi);
    IOH_New8(REG_SPCR2(base),spcr2_read,spcr2_write,rspi);
    IOH_New16(REG_SPCMD0(base),spcmd0_read,spcmd0_write,rspi);
    IOH_New16(REG_SPCMD1(base),spcmd1_read,spcmd1_write,rspi);
    IOH_New16(REG_SPCMD2(base),spcmd2_read,spcmd2_write,rspi);
    IOH_New16(REG_SPCMD3(base),spcmd3_read,spcmd3_write,rspi);
    IOH_New16(REG_SPCMD4(base),spcmd4_read,spcmd4_write,rspi);
    IOH_New16(REG_SPCMD5(base),spcmd5_read,spcmd5_write,rspi);
    IOH_New16(REG_SPCMD6(base),spcmd6_read,spcmd6_write,rspi);
    IOH_New16(REG_SPCMD7(base),spcmd7_read,spcmd7_write,rspi);
}


BusDevice *
RxRSpi_New(const char *name) 
{
    RxRSpi *rspi = sg_new(RxRSpi);
    int i;
    rspi->bdev.first_mapping = NULL;
    rspi->bdev.Map = Rspi_Map;
    rspi->bdev.UnMap = Rspi_Unmap;
    rspi->bdev.owner = rspi;
    rspi->bdev.hw_flags = MEM_FLAG_WRITABLE | MEM_FLAG_READABLE;
    rspi->sigMosi = SigNode_New("%s.mosi", name);
    rspi->sigMiso = SigNode_New("%s.miso", name);
    rspi->sigSclk = SigNode_New("%s.sclk", name);
    //rspi->sigShiftOut = SigNode_New("%s.shiftOut", name);
    //rspi->sigShiftIn = SigNode_New("%s.shiftIn", name);
    rspi->clkIn = Clock_New("%s.clk", name);
    rspi->clkBase = Clock_New("%s.clk_base", name);
    rspi->regSPBR = 255;
    Clock_MakeDerived(rspi->clkBase, rspi->clkIn, 1, 2 * 256);
    if (!rspi->sigMosi || !rspi->sigMiso || !rspi->sigSclk) {
        fprintf(stderr,"Can not create signals for SPI controller\n");
        exit(1);
    }
    for (i = 0; i < array_size(rspi->sigSSL) ; i++) {
        rspi->sigSSL[i] = SigNode_New("%s.ssl%u", name, i);
        if (!rspi->sigSSL[i]) {
            fprintf(stderr,"Can not create signals for SPI controller\n");
            exit(1);
        }
    }
    rspi->sigIrqSpti = SigNode_New("%s.irqSpti", name);
    rspi->sigIrqSpri = SigNode_New("%s.irqSpri", name);
    rspi->sigIrqSpii = SigNode_New("%s.irqSpii", name);
    rspi->sigIrqSpei = SigNode_New("%s.irqSpei", name);
    if (!rspi->sigIrqSpti || !rspi->sigIrqSpri || !rspi->sigIrqSpii || !rspi->sigIrqSpei) {
        fprintf(stderr,"Can not create Interrupts for SPI controller\n");
        exit(1);
    }
    SigNode_Set(rspi->sigIrqSpti, SIG_HIGH);  /* Edge interrupt (neg) */
    SigNode_Set(rspi->sigIrqSpri, SIG_HIGH);  /* Edge interrupt (neg) */
    SigNode_Set(rspi->sigIrqSpii, SIG_HIGH);  /* Level interrupt (low) */
    SigNode_Set(rspi->sigIrqSpei, SIG_HIGH);  /* Group interrupt */
    CycleTimer_Init(&rspi->instrDelayTimer, RxSpi_InterpLoop, rspi); 
    fprintf(stderr, "Created RSPI \"%s\"\n", name);
    return &rspi->bdev;
}
